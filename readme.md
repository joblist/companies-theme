# `companies-theme`

## Default installation as a gohugo theme

Follow instructions on [gohugo.io/docs/themes](https://gohugo.io/hosting-and-deployment/deployment-with-nanobox/#install-a-theme).

## Installation with "minimal folder structure"

> This configuration tries to have a minimal number file/folder at the
> root of the repository (we'd want to have only the content as
> visible folder/filers, in the best case; and no config/theme).

To install this theme in a `my-project` new empty project folder:

```bash
mkdir my-project
cd my-project
git init .
mkdir .themes
touch .themes/config.toml
git submodule add git@gitlab.com:joblist/companies-theme.git .themes/companies-theme
hugo server --config="./.themes/config.toml"
```

Note: if you install the theme with `git@gitlab...` instead of
`https://gitlab.com/...`, you won't be able to clone the submodule in
the Gitlab CI context. 

You can then populate the `./themes/.config.toml` file like so:
```
title = "Companies"
contentDir = "."
theme = "companies-theme"
themesDir = ".themes"
baseURL = ""
languageCode = "en-us"

[taxonomies]
tag = "tags"

[Params]
editNetlifyCmsUrl = "https://companies.joblist.city/edit/#/collections"
```
> Note: we specify taxonomy to hide the default "categories", and only show "tags"; it's still gohugo theme!

## Development

Serve the local hugo server:

```
hugo server --config="./.themes/config.toml"
```

> To work within a theme installed in a site (with `https`), you can
> add a second git remote (with `ssh`), to be able to push with ssh
> key (and no email/pw credentials).

## Deployment

To build a version of the theme

```
hugo --config="./.themes/config.toml" --baseURL="https://companies.joblist.city"
```
